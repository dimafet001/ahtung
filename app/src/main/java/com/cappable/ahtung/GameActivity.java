package com.cappable.ahtung;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by dimafet on 31.08.15.
 */
public class GameActivity extends Activity implements FragmentManager.OnBackStackChangedListener{
    private static int screenH, screenW;
    private final static String SP_SCREEN_WIDTH = "scr_w";
    private final static String SP_SCREEN_HEIGHT = "scr_h";
    GameBoard gb;
    LinearLayout controlsPlayer1, controlsPlayer2;
    Button btnLeft1, btnRight1
            ,additionalBtnLeft1, additionalBtnRight1
            ,additionalBtnLeft2, additionalBtnRight2
            ,additionalBtnLeftSolo, additionalBtnRightSolo
            ,btnConfirm1, btnConfirm2, btnConfirmSolo
            ,btnPause;
    Button btnAddPlayer;
    Button btnPlay, btnPlayPvP;
    TextView tvScore;
    FrameLayout frameControls2;//controls for 2 player

    Animation alphaToMinBtnPlay, alphaToMinBtnPlayPvp, alphaToMax,
            alphaToMinConfirm, alphaToMinConfirmSolo, shakeBtnSolo, shakeBtnPvP;

    private boolean isGamePlay = false;
    private boolean twoPlayerMode = false;
    private boolean[] confirmed = {false, false};
    private boolean doubleBackPressed = false;
    private boolean isCheckingReadyOrNot = false;//the moment after you pressed on play btn and app
    //waits till you r ready
    private boolean mShowingBack = false;

    CardBackFragment cbf = new CardBackFragment();
    CardFrontFragment cff = new CardFrontFragment();
    private FrameLayout frameBlurred;
    private boolean isPaused = false;// = frameDarken is out

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("activity", "create");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        screenH = sp.getInt(SP_SCREEN_HEIGHT, 1000);
        screenW = sp.getInt(SP_SCREEN_WIDTH, 1000);

        if (savedInstanceState == null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame_player_2, cff)
                    .commit();
        } else {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }
        getFragmentManager().addOnBackStackChangedListener(this);

        gb = (GameBoard) findViewById(R.id.gb);
        btnLeft1 = (Button) findViewById(R.id.left1);
        btnRight1 = (Button) findViewById(R.id.right1);
        additionalBtnLeft1 = (Button) findViewById(R.id.btn_additional_player1_l);
        additionalBtnRight1 = (Button) findViewById(R.id.btn_additional_player1_r);
        additionalBtnLeft2 = (Button) findViewById(R.id.btn_additional_player2_l);
        additionalBtnRight2 = (Button) findViewById(R.id.btn_additional_player2_r);
        additionalBtnLeftSolo = (Button) findViewById(R.id.btn_additional_player_solo_l);
        additionalBtnRightSolo = (Button) findViewById(R.id.btn_additional_player_solo_r);
        frameControls2 = (FrameLayout) findViewById(R.id.frame_player_2);
        frameBlurred = (FrameLayout) findViewById(R.id.blurredArea);
        controlsPlayer1 = (LinearLayout) findViewById(R.id.layout_player_1);
        btnAddPlayer = (Button) findViewById(R.id.btn_add_player);
        btnConfirm1 = (Button) findViewById(R.id.btn_confirm_1);
        btnConfirm2 = (Button) findViewById(R.id.btn_confirm_2);
        btnConfirmSolo = (Button) findViewById(R.id.btn_confirm_solo);
        btnPlay = (Button) findViewById(R.id.btn_play);
        btnPlayPvP = (Button) findViewById(R.id.btn_play_two);
        btnPause = (Button) findViewById(R.id.btn_pause);
        tvScore = (TextView) findViewById(R.id.tv_score);
//        tvScore.setRotation(90);
        btnConfirm2.setRotation(180);

        alphaToMinBtnPlay = AnimationUtils.loadAnimation(this, R.anim.alpha_to_min);//listened
        alphaToMinBtnPlayPvp = AnimationUtils.loadAnimation(this, R.anim.alpha_to_min);//listened
        alphaToMinConfirm = AnimationUtils.loadAnimation(this, R.anim.alpha_to_min);//listened
        alphaToMinConfirmSolo = AnimationUtils.loadAnimation(this, R.anim.alpha_to_min);//listened
        alphaToMax = AnimationUtils.loadAnimation(this, R.anim.alpha_to_max);
        shakeBtnSolo = AnimationUtils.loadAnimation(this, R.anim.shake_btn_solo);//listened
        shakeBtnPvP = AnimationUtils.loadAnimation(this, R.anim.shake_btn_solo);

        gb.setScoresTextView(tvScore);
        gb.resume();
        frameBlurred.setLeft(frameBlurred.getRight() * (-1));
        frameBlurred.setRight(0);


        // ------------------------------ controls ------------------------------------- //

        View.OnTouchListener touchPlayer1 = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    if (v.getId() == R.id.left1) {
                        gb.turnLeft = true;
                        gb.turnRight = false;
                    } else {
                        gb.turnRight = true;
                        gb.turnLeft = false;
                    }
                else if (event.getAction() == MotionEvent.ACTION_UP)
                    if (v.getId() == R.id.left1) gb.turnLeft = false;
                    else gb.turnRight = false;
                return false;
            }
        };
        //only for btn_additional_1_(r/l)
        View.OnTouchListener touchAdditional1 = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isGamePlay) {
                    if (v.getId() == R.id.btn_additional_player1_l
                            || v.getId() == R.id.btn_additional_player_solo_l)
                        btnLeft1.dispatchTouchEvent(event);
                    else
                        btnRight1.dispatchTouchEvent(event);

                }
                return false;
            }
        };
        //only for btn_additional_2_(r/l)
        View.OnTouchListener touchAdditional2 = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isGamePlay) {
                    if (v.getId() == R.id.btn_additional_player2_l)
                        cbf.btnLeft2.dispatchTouchEvent(event);
                    else
                        cbf.btnRight2.dispatchTouchEvent(event);
                }
                return false;
            }
        };

        btnLeft1.setOnTouchListener(touchPlayer1);
        btnRight1.setOnTouchListener(touchPlayer1);
        additionalBtnLeft1.setOnTouchListener(touchAdditional1);
        additionalBtnRight1.setOnTouchListener(touchAdditional1);
        additionalBtnLeftSolo.setOnTouchListener(touchAdditional1);
        additionalBtnRightSolo.setOnTouchListener(touchAdditional1);
        additionalBtnLeft2.setOnTouchListener(touchAdditional2);
        additionalBtnRight2.setOnTouchListener(touchAdditional2);

        // ------------------------------ controls  end -------------------------------- //

        // ------------------------------ animations ------------------------------------- //

        alphaToMinBtnPlay.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                btnPlay.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btnPlay.setVisibility(View.GONE);
            }
            public void onAnimationRepeat(Animation animation) {}
        });
        alphaToMinBtnPlayPvp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                btnPlayPvP.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btnPlayPvP.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        alphaToMinConfirm.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btnConfirm1.setVisibility(View.GONE);
                btnConfirm2.setVisibility(View.GONE);
                btnConfirm1.setClickable(false);
                btnConfirm2.setClickable(false);
                // setting checking for readiness views to unready
                setConfirmToUnready(false);
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        alphaToMinConfirmSolo.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btnConfirmSolo.setVisibility(View.GONE);
                btnConfirmSolo.setClickable(false);
                // setting checking for readiness views to unready
                setConfirmToUnready(true);
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        shakeBtnSolo.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                Animation minmax = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha_to_min);
                minmax.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        btnPlayPvP.setVisibility(View.VISIBLE);
                        btnPlayPvP.setClickable(true);
                        btnPlayPvP.startAnimation(alphaToMax);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        btnPlay.setClickable(false);
                        btnPlay.setVisibility(View.GONE);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                btnPlay.startAnimation(minmax);
            }
            public void onAnimationRepeat(Animation animation) {}
        });
        shakeBtnPvP.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                Animation minmax = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha_to_min);
                minmax.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        btnPlay.setVisibility(View.VISIBLE);
                        btnPlay.setClickable(true);
                        btnPlay.startAnimation(alphaToMax);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        btnPlayPvP.setClickable(false);
                        btnPlayPvP.setVisibility(View.GONE);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                btnPlayPvP.startAnimation(minmax);
            }
            public void onAnimationRepeat(Animation animation) {}
        });

        // ------------------------------ animations end --------------------------------- //

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isGamePlay) {
                    if (!twoPlayerMode) {
                        //single player
                        btnConfirmSolo.setVisibility(View.VISIBLE);
                        btnConfirmSolo.setClickable(true);
                        btnConfirmSolo.startAnimation(alphaToMax);
                        btnPlay.startAnimation(alphaToMinBtnPlay);
                        frameControls2.animate().translationY(-frameControls2.getHeight());
                        tvScore.animate().translationY(-frameControls2.getHeight());
                        frameBlurred.setTop(0);
                        GameBoard.upperBorder = 0;

                        isCheckingReadyOrNot = true;
                    }
                }
            }
        });
        btnPlayPvP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isGamePlay) {
                    if (twoPlayerMode) {
                        //pvp
                        btnConfirm1.setVisibility(View.VISIBLE);
                        btnConfirm1.setClickable(true);
                        btnConfirm2.setVisibility(View.VISIBLE);
                        btnConfirm2.setClickable(true);
                        btnConfirm1.startAnimation(alphaToMax);
                        btnConfirm2.startAnimation(alphaToMax);
                        btnPlayPvP.startAnimation(alphaToMinBtnPlayPvp);
                        GameBoard.upperBorder = getResources().getDimensionPixelSize(R.dimen.frame_add_player_height);

                        isCheckingReadyOrNot = true;
                    }
                }
            }
        });

        View.OnClickListener confirm = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isGamePlay) {
                    if (twoPlayerMode) {
                        if (v.getId() == R.id.btn_confirm_1) {
                            if (!confirmed[0]) {
                                confirmed[0] = true;
                                btnConfirm1.setText(getText(R.string.ready)+"\n"+getText(R.string.waiting_for_player));
                            }
                        } else {
                            if (!confirmed[1]) {
                                confirmed[1] = true;
                                btnConfirm2.setText(getText(R.string.ready)+"\n"+getText(R.string.waiting_for_player));
                            }
                        }
                        OnBothConfirmedCheck();
                    } else {//single game mode
                        btnConfirmSolo.setText(getText(R.string.ready));
                        isGamePlay = true;
                        btnConfirmSolo.startAnimation(alphaToMinConfirmSolo);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                additionalBtnLeftSolo.setClickable(true);
                                additionalBtnLeftSolo.setVisibility(View.VISIBLE);
                                additionalBtnRightSolo.setClickable(true);
                                additionalBtnRightSolo.setVisibility(View.VISIBLE);
                                btnPause.setVisibility(View.VISIBLE);
                                btnPause.animate().translationXBy(screenW/2).setDuration(100);
                                gb.startGame();
                            }
                        }, 400);
                    }
                }
            }
        };

        btnConfirm1.setOnClickListener(confirm);
        btnConfirm2.setOnClickListener(confirm);
        btnConfirmSolo.setOnClickListener(confirm);

        // ------------------- setting visibility of what we dont need
        btnPlayPvP.setClickable(false);
        btnConfirm1.setClickable(false);
        btnConfirm2.setClickable(false);
        btnConfirmSolo.setClickable(false);
        additionalBtnLeft1.setClickable(false);
        additionalBtnRight1.setClickable(false);
        additionalBtnLeft2.setClickable(false);
        additionalBtnRight2.setClickable(false);
        additionalBtnRightSolo.setClickable(false);
        additionalBtnLeftSolo.setClickable(false);
        btnConfirm1.setVisibility(View.GONE);
        btnConfirm2.setVisibility(View.GONE);
        btnConfirmSolo.setVisibility(View.GONE);
        btnPlayPvP.setVisibility(View.GONE);
        additionalBtnLeft1.setVisibility(View.GONE);
        additionalBtnRight1.setVisibility(View.GONE);
        additionalBtnLeft2.setVisibility(View.GONE);
        additionalBtnRight2.setVisibility(View.GONE);
        additionalBtnRightSolo.setVisibility(View.GONE);
        additionalBtnLeftSolo.setVisibility(View.GONE);

        frameBlurred.setTop(frameControls2.getHeight());
        frameBlurred.setVisibility(View.INVISIBLE);
        frameBlurred.animate().translationXBy(screenW * -1);
        btnPause.setVisibility(View.INVISIBLE);
        btnPause.animate().translationXBy(screenW/2*-1);
    }

    private void OnBothConfirmedCheck() {
        if (confirmed[0] && confirmed[1]) {
            btnConfirm1.setText(getText(R.string.ready));
            btnConfirm2.setText(R.string.ready);
            isGamePlay = true;
            btnConfirm1.startAnimation(alphaToMinConfirm);
            btnConfirm2.startAnimation(alphaToMinConfirm);
            confirmed[0] = confirmed[1] = false;
            additionalBtnLeft1.setClickable(true);
            additionalBtnLeft1.setVisibility(View.VISIBLE);
            additionalBtnRight1.setClickable(true);
            additionalBtnRight1.setVisibility(View.VISIBLE);
            additionalBtnLeft2.setClickable(true);
            additionalBtnLeft2.setVisibility(View.VISIBLE);
            additionalBtnRight2.setClickable(true);
            additionalBtnRight2.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    btnPause.setVisibility(View.VISIBLE);
                    btnPause.animate().translationXBy(screenW/2).setDuration(200);
                    gb.startGame();
                }
            }, 400);
        }
    }

    private void setConfirmToUnready(boolean solo) {
        // setting checking for readiness views to unready
        if (solo)
            btnConfirmSolo.setText(getResources().getText(R.string.not_ready));
        else {
            confirmed[0] = confirmed[1] = false;
            btnConfirm1.setText(getResources().getText(R.string.not_ready));
            btnConfirm2.setText(getResources().getText(R.string.not_ready));
        }
    }

    public void addPlayer(View v) {//method calls when addPlayerBtn is clicked
//        controlsPlayer2.setVisibility(View.VISIBLE);
//        btnAddPlayer.setVisibility(View.GONE);
        twoPlayerMode = true;
        flipCard();
        btnPlay.startAnimation(shakeBtnSolo);
        tvScore.animate().translationX(screenW - tvScore.getHeight() - tvScore.getLeft())
                .translationY((screenH - tvScore.getWidth()) / 2 - tvScore.getTop()
                        - getResources().getDimensionPixelSize(R.dimen.status_bar) / 2)
                .rotation(90).setDuration(400)
                .setInterpolator(new AccelerateDecelerateInterpolator());
    }

    //-----------------------------------------------------------------//
    private void flipCard() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }

        // Flip to the back.
        mShowingBack = true;

        // Create and commit a new fragment transaction that adds the fragment for the back of
        // the card, uses custom animations, and is part of the fragment manager's back stack.

        getFragmentManager()
                .beginTransaction()

                        // Replace the default fragment animations with animator resources representing
                        // rotations when switching to the back of the card, as well as animator
                        // resources representing rotations when flipping back to the front (e.g. when
                        // the system Back button is pressed).
                .setCustomAnimations(
                        R.anim.card_flip_right_in, R.anim.card_flip_right_out,
                        R.anim.card_flip_left_in, R.anim.card_flip_left_out)

                        // Replace any fragments currently in the container view with a fragment
                        // representing the next page (indicated by the just-incremented currentPage
                        // variable).
                .replace(R.id.frame_player_2, cbf)

                        // Add this transaction to the back stack, allowing users to press Back
                        // to get to the front of the card.
                .addToBackStack(null)

                        // Commit the transaction.
                .commit();
    }

    @Override
    public void onBackStackChanged() {
        mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
    }

    public void onPauseGame(View v) {
        isPaused = !isPaused;
//        gb.isPaused = true;
        gb.isPaused = !gb.isPaused;
        frameBlurred.setVisibility(View.VISIBLE);
        frameBlurred.animate().translationX(isPaused ? 0 : screenW * -1)//translate left to x by the start point of view
                .setDuration(800).setInterpolator(new AccelerateDecelerateInterpolator());
        btnPause.animate().translationX(isPaused ? screenW : 0)//translate left to
                .setDuration(800).setInterpolator(new AccelerateDecelerateInterpolator());
    }

    private static void setClickVis(View v, boolean visible) {
        if (visible) {
            v.setVisibility(View.VISIBLE);
            v.setClickable(true);
        } else {
            v.setClickable(false);
            v.setVisibility(View.GONE);
        }
    }

    //---------------lower only activity methods--------------------------//
    @Override
    public void onBackPressed() {
//        Log.d("activity", "backPress");
        if (isGamePlay) {
            btnPause.performClick();
        } else if (isCheckingReadyOrNot && !isGamePlay) {//moment when there are views "ready?"
            isCheckingReadyOrNot = false;
            if (!twoPlayerMode) {//singleplayer
                btnPlay.setVisibility(View.VISIBLE);
                btnPlay.startAnimation(alphaToMax);
                btnPlay.setClickable(true);
                btnConfirmSolo.startAnimation(alphaToMinConfirmSolo);
                frameControls2.animate().translationY(0);
                tvScore.animate().translationY(0).setDuration(300);
                frameBlurred.setTop(frameControls2.getHeight());
            } else {//PvP
                btnConfirm1.startAnimation(alphaToMinConfirm);
                btnConfirm2.startAnimation(alphaToMinConfirm);
                btnPlayPvP.setVisibility(View.VISIBLE);
                btnPlayPvP.startAnimation(alphaToMax);
                btnPlayPvP.setClickable(true);
            }
        } else if (doubleBackPressed) {
            if (mShowingBack)
                flipCard();
            super.onBackPressed();
        } else if (mShowingBack && !isGamePlay) {//returning from PvP to single mode
            twoPlayerMode = false;
            btnPlayPvP.startAnimation(shakeBtnPvP);
            tvScore.animate().translationX(0)
                    .translationY(0)
                    .rotation(0).setDuration(400);
            super.onBackPressed();
        } else {
            doubleBackPressed = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackPressed = false;
                }
            }, 2000);
        }
    }

    @Override
    protected void onPause() {
        Log.d("activity", "pause");
        gb.pause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.d("activity", "destroy");
        gb.destroy();
        super.onDestroy();
    }

    /**
     * A fragment representing the front of the card.
     */
    @SuppressLint("ValidFragment")
    public class CardFrontFragment extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.btn_add_player, container, false);
        }
    }

    /**
     * A fragment representing the back of the card.
     */
    @SuppressLint("ValidFragment")
    public class CardBackFragment extends Fragment {
        Button btnRight2, btnLeft2;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View root = inflater.inflate(R.layout.layout_controls_player_2, container, false);
            btnRight2 = (Button) root.findViewById(R.id.right2);
            btnLeft2 = (Button) root.findViewById(R.id.left2);

            View.OnTouchListener touchPlayer2 = new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                        if (v.getId() == R.id.left2) {
                            gb.turn_left_2 = true;
                            gb.turn_right_2 = false;
                        } else {
                            gb.turn_right_2 = true;
                            gb.turn_left_2 = false;
                        }
                    else if (event.getAction() == MotionEvent.ACTION_UP)
                        if (v.getId() == R.id.left2) gb.turn_left_2 = false;
                        else gb.turn_right_2 = false;
                    return false;
                }
            };
            btnRight2.setOnTouchListener(touchPlayer2);
            btnLeft2.setOnTouchListener(touchPlayer2);

            return root;
        }
    }
}
