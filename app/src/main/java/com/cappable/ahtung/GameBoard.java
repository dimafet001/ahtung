package com.cappable.ahtung;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Parcel;
import android.preference.PreferenceManager;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Random;

/**
 * Created by dimafet on 31.08.15.
     */
public class GameBoard extends SurfaceView implements Runnable, SurfaceHolder.Callback{
    private static int screenH, screenW;
    protected static int upperBorder = 0;
    private final static String SP_SCREEN_WIDTH = "scr_w";
    private final static String SP_SCREEN_HEIGHT = "scr_h";
    Thread thread, collisionThread;
    TextView scoresTextView;

    //Game Vars
    boolean turnLeft = false, turnRight = false, //for lower snake
    turn_left_2 = false, turn_right_2 = false, //for upper snake (Comp or Player 2)
    gameOver = false;
    private static int[] scorePoints = {0, 0};//score points for each player
    private static final int player1 = 0, player2 = 1, draw = 2;

    //draw vars
    Canvas canvas;
    SurfaceHolder holder;
    Snake[] snakes = new Snake[2];
    private int bgColor;
    private boolean isRun = true, isOK = false, wasGameOverUpdate = false, isStarting = true;
    protected boolean isPaused = true;

    float last_time, new_time, fps = 1000/60f;
    float last_time_col, new_time_col;
    private int rotate_degree = 3;
    private Handler handler = new Handler();
    Bitmap cBitmap;

//    RenderScript

    public GameBoard(Context context) {
        super(context);
        init();
    }

    public GameBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GameBoard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        holder = getHolder();
        holder.addCallback(this);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getContext());
//        screenH = sp.getInt(SP_SCREEN_HEIGHT, 1000);
        reset();

        collisionThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (isRun) {
                    while (isOK) {
                        last_time_col = System.currentTimeMillis();
                        try {
                            if (!isPaused)
                                updCollisions();
                        } catch (IndexOutOfBoundsException e) {
                            e.printStackTrace();
                        }
                        new_time_col = System.currentTimeMillis();
                        if (last_time_col + fps > new_time_col)
                            try {
                                collisionThread.sleep((long) (last_time_col + fps - new_time_col));
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                    }
                }
            }
        });

        bgColor = getResources().getColor(R.color.bg);
    }

    @Override
    public void run() {
        while (isRun) {
            while (isStarting) {
                if (!holder.getSurface().isValid()) continue;
                last_time = System.currentTimeMillis();
                canvas = holder.lockCanvas();
                try {
                    canvas.drawColor(bgColor);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                try {
                    holder.unlockCanvasAndPost(canvas);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                new_time = System.currentTimeMillis();
                if (last_time + fps > new_time)
                    try {
                        Thread.sleep((long) (last_time + fps - new_time));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
            while (isOK) {
                if (!holder.getSurface().isValid()) continue;
//                Log.d("loop", "really run game loop");
                last_time = System.currentTimeMillis();
                canvas = holder.lockCanvas();
                //DRAWINGS AND UPDATES HERE IN MAIN LOOP THREAD
                if (!isPaused)
                    update();
                try {
//                    if (!isPaused) {
                        canvas.drawColor(bgColor);
                        snakes[0].draw(canvas);
                        snakes[1].draw(canvas);
//                    } else {
//                    if (isPaused) {
//                        canvas.drawColor(getResources().getColor(R.color.pause), PorterDuff.Mode.DST_ATOP);//(150, 10, 10, 10);
//                        todo
//                        Paint p = new Paint();
//                        p.setMaskFilter(new BlurMaskFilter(1000, BlurMaskFilter.Blur.INNER));

//                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                //------------------------------------------
                try {
                    holder.unlockCanvasAndPost(canvas);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                new_time = System.currentTimeMillis();
                if (last_time + fps > new_time)
                    try {
                        Thread.sleep((long) (last_time + fps - new_time));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
        }
    }

    private void update() {
        if (turnLeft) {
            snakes[0].degree -= rotate_degree;
        } else if (turnRight) {
            snakes[0].degree += rotate_degree;
        }
        if (turn_left_2) {
            snakes[1].degree -= rotate_degree;
        } else if (turn_right_2) {
            snakes[1].degree += rotate_degree;
        }

        snakes[0].update();
        snakes[1].update();
    }

    private void updCollisions() {
        try {
            for (int i = 0; i < snakes.length; ++i)
            if (snakes[i].x + snakes[i].halfSize > screenW
                    || snakes[i].x - snakes[i].halfSize < 0
                    || snakes[i].y + snakes[i].halfSize > screenH
                    || snakes[i].y - snakes[i].halfSize < upperBorder) {
                gameOver(i == 0 ? player2 : player1);
                return;
            }
            if (Math.hypot(snakes[0].x - snakes[1].x, snakes[0].y - snakes[1].y) <= snakes[0].size) {
                gameOver(draw);
            } else {
                for (Point p : snakes[1].points) {
                    if (Math.hypot(snakes[0].x - p.x, snakes[0].y - p.y) <= snakes[0].size) {
                        gameOver(player2);
                        return;
                    }
                }
                for (Point p : snakes[0].points) {
                    if (Math.hypot(snakes[1].x - p.x, snakes[1].y - p.y) <= snakes[1].size) {
                        gameOver(player1);
                        return;
                    }
                }
                //if snake[0] caught itself
                for (int i = 0; i < snakes[0].points.size() - 20; ++i) {
                    if (Math.hypot(snakes[0].x - snakes[0].points.get(i).x,
                            snakes[0].y - snakes[0].points.get(i).y) <= snakes[0].size) {
                        gameOver(player2);
                        return;
                    }
                }
                //if snake[1] caught itself
                for (int i = 0; i < snakes[1].points.size() - 20; ++i) {
                    if (Math.hypot(snakes[1].x - snakes[1].points.get(i).x,
                            snakes[1].y - snakes[1].points.get(i).y) <= snakes[1].size) {
                        gameOver(player1);
                        return;
                    }
                }
            }
        } catch (ConcurrentModificationException e) {}
    }

    private void gameOver(int winner) {
        if (!wasGameOverUpdate) {
            wasGameOverUpdate = true;
            Log.d("game over: ", "" + winner);
            if (winner != 2) {
                ++scorePoints[winner];
                if (scorePoints[winner] == 20)
                    Arrays.fill(scorePoints, 0);
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    scoresTextView.setText(scorePoints[1] + ":" + scorePoints[0]);
                }
            });

            gameOver = isPaused = true;

            //ToDo: Right here add explotion and animate point increment
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    reset();
                    isPaused = false;
                }
            }, 500);
        }
    }

    public void startGame() {
        reset();
        isPaused = false;
        isStarting = false;
        isOK = true;
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }, 300);
    }

    protected void reset() {
        int m = getResources().getDimensionPixelSize(R.dimen.margin_snake_start);
        snakes[0] = new Snake(m, screenH - m*1.5f, -90,
                (float) Math.hypot(screenH, screenW) / 500f, getResources().getColor(R.color.red));
        snakes[1] = new Snake(screenW - m, m  + upperBorder, 90,
                (float) Math.hypot(screenH, screenW) / 500f, getResources().getColor(R.color.blue));
        wasGameOverUpdate = gameOver = false;
    }

    public void pause() {
        isOK = false;
        isPaused = true;
//        try {
//            thread.join();
//            collisionThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    public void resume() {
        isRun = true;
        isOK = false;
        isPaused = true;
        thread = new Thread(this);
        thread.start();
        collisionThread.start();
//        isStartScreen = true;
    }

    public void destroy() {
        isRun = false;
        isOK = false;
        isPaused = true;
        Arrays.fill(scorePoints, 0);
//        try {
//            thread.join();
//            collisionThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        thread = collisionThread = null;

    }

    public void setScoresTextView(TextView scoresTextView) {
        this.scoresTextView = scoresTextView;
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Rect r = holder.getSurfaceFrame();
        screenH = r.height();
//        screenW = sp.getInt(SP_SCREEN_WIDTH, 1000);
        screenW = r.width();

        Log.d("width & height", screenW + "   " + screenH);

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                cBitmap = Bitmap.createBitmap(screenW, screenH, Config.ARGB_4444);
//                canvas.setBitmap(cBitmap);
//            }
//        }, 2000);
        isOK = true;
        isPaused = true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Rect r = holder.getSurfaceFrame();
        screenH = r.height();
//        screenW = sp.getInt(SP_SCREEN_WIDTH, 1000);
        screenW = r.width();

        Log.d("width & height", screenW + "   " + screenH);

        isOK = true;
        isPaused = true;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    class Snake{
        float x, y, speed;//x , y are center of form
        int degree = 90;//up dir
        float size, halfSize;
        Paint p = new Paint(), p_interrupted_circle = new Paint();
        ArrayList<Point> points = new ArrayList<>();
        ArrayList<Path> paths = new ArrayList<>();
        int upd = 0;//to make array of points less
        int random_num_of_frames_to_interrupt_path = 1;
        int random_num_of_frames_to_start_path = 1;
        boolean isInterrupted = false, isStarted = true;

        public Snake(float x, float y, int degree,float speed, int color){
            this.x = x;
            this.y = y;
            this.speed = speed;
            this.degree = degree;
            size = 25;
            halfSize = size / 2;
            p.setColor(color);
            p.setStrokeWidth(size);
            p.setStyle(Paint.Style.STROKE);
            p.setDither(true);
            p.setStrokeJoin(Paint.Join.ROUND);
            p.setStrokeCap(Paint.Cap.ROUND);
            p.setPathEffect(new CornerPathEffect(10));
            p.setAntiAlias(true);
            p_interrupted_circle.setColor(color);
            paths.add(new Path());
//            points.add(new Point((int) this.x, (int) this.y));
            paths.get(0).moveTo(x, y);
            random_num_of_frames_to_interrupt_path = getRandInterrupt();
            random_num_of_frames_to_start_path = getRandStart();
        }

        public void update() {
            float plusX = (float) Math.cos(Math.toRadians(degree)) * speed;
            float plusY = (float) Math.sin(Math.toRadians(degree)) * speed;
            x += plusX;
            y += plusY;
            if ((++upd > 3) && !isInterrupted) {
                upd = 0;
                points.add(new Point((int) this.x, (int) this.y));
            }
            if (!isInterrupted && (--random_num_of_frames_to_interrupt_path < 1)) {//waiting for interrupting
                isInterrupted = true;
                isStarted = false;
                random_num_of_frames_to_interrupt_path = getRandInterrupt();
                paths.add(new Path());
                paths.get(paths.size() - 1).moveTo(x, y);
            } else if (!isStarted && (--random_num_of_frames_to_start_path < 1)) {//waiting for continue
                isInterrupted = false;
                isStarted = true;
                random_num_of_frames_to_start_path = getRandStart();
            } else if (isInterrupted){//drawing interrupted
                paths.get(paths.size() - 1).moveTo(x, y);
            } else if (isStarted)
                paths.get(paths.size() - 1).lineTo(x, y);
        }
        public void draw(Canvas canvas) {
            for (Path path: paths) {
                canvas.drawPath(path, p);
            }
            if (isInterrupted && !isStarted) {
                canvas.drawCircle(x, y - halfSize *9/16, size * 9/16f, p_interrupted_circle);
            }
        }

        private int getRandInterrupt() {
            return new Random().nextInt(250) + 20;
        }

        private int getRandStart() {
            return new Random().nextInt(15) + 20;
        }
    }



    //------------------------------------------   AI   ------------------------------------------//

    final Thread AI = new Thread(new Runnable() {
        @Override
        public void run() {
            //AI ALGORITHM
            long last_time, new_time;
            float[] radiusL = new float[2];
            float[] radiusR = new float[2];

            while(isRun) {
                while (isOK) {
                    last_time = System.currentTimeMillis();

                    //Whole code is here
                    //find angle speed
                    //------------------

                    new_time = System.currentTimeMillis();
                    if (last_time + fps > new_time)
                        try {
                            AI.sleep((long) (last_time + fps - new_time));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                }
            }
        }
    });

    private static final int l = 0, r = 1, s = 2;
    private void changeDirAI(int dir) {
        if (dir == 1) {
            turn_left_2 = true;
            turn_right_2 = false;
        } else if (dir == 0) {
            turn_right_2 = true;
            turn_left_2 = false;
        } else {
            turn_left_2 = turn_right_2 = false;
        }
    }
}
