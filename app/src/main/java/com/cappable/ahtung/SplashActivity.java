
package com.cappable.ahtung;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;

public class SplashActivity extends AppCompatActivity {
    final static String SP_SCREEN_WIDTH = "scr_w";
    final static String SP_SCREEN_HEIGHT = "scr_h";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final RelativeLayout splash = (RelativeLayout) findViewById(R.id.splash);

        new Handler().postDelayed(new Runnable() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void run() {

                if (!sp.contains(SP_SCREEN_HEIGHT)) {
                    sp.edit().putInt(SP_SCREEN_HEIGHT, getResources().getDisplayMetrics().heightPixels).commit();
//                    mSharedPreferences.edit().putInt(SP_SCREEN_WIDTH, getWindow().getDecorView().getWidth()).commit();
                    sp.edit().putInt(SP_SCREEN_WIDTH, getResources().getDisplayMetrics().widthPixels).commit();
                }
                startActivity(new Intent(SplashActivity.this, GameActivity.class));
                finish();
                //ToDo: next change 500 to 1500
            }
        }, 500);
    }
}
